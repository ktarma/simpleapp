// + Code has a copyright statement.
/*
 * Copyright (C) 2014 Kaido Tarma
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package ee.motive.simpleapp.other;

import android.util.Log;

import ee.motive.simpleapp.Config;

// + Class is documented.
// + There are no unused non-public variables or methods.
/**
 * Does nothing important, just to show the style guidelines.<br>
 * All comments starting with a "+" are describing some good practice.
 */
public class CodeStyleGood {

    // + All variables are initialized.
    // + Final fields have all capital letters.
    // + Public fields are not using a prefix.
    // + Static field is using "s"-prefix.
    // + Non-public, non-static fields are using "m"-prefix.
    private static final boolean LOCAL_LOG = Config.DEBUG;
    public static final int SOME_CONSTANT = 5;
    public int publicField = 5;
    private static int sStaticField = 5;
    int mPackagePrivateField = 5;
    private int mPrivateField = 5;
    protected int mProtectedField = 5;

    // + A boolean returning method is using "is"-prefix.
    private boolean isThisBetterMethod() {
        return true;
    }

    // + Public method is documented.
    /**
     * Gets an URL to a great school.
     * 
     * @return An URL.
     */
    public String getUrl() {
        return "http://www.itcollege.ee/";
    }

    // + Method is catching specific exceptions (not just a generic exception).
    // + Exception is not ignored.
    // + Using a TODO remark for code that needs to be changed later.
    void setPrivateField(String value) {
        try {
            mPrivateField = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            // TODO: Explain why the field should be 0.
            mPrivateField = 0;
        }
    }

    // + Following indentation rules.
    // + Using braces for if statements.
    // + Variables are declared in the innermost block that
    // encloses all uses of the variable.
    // + "{" is with previous line, not on its own line.
    private void checkIndentation() {
        if (true) {
            boolean scopeVariable = true;
            if (true) {
                if (!false) {

                }
            }
            if (scopeVariable) {

            }
        }
    }

    // + Logging using a final LOCAL_LOG variable, so it can be compiled out.
    /** Method to use all unused methods and variables. */
    void useVariablesAndMethods() {
        if (LOCAL_LOG) {
            Log.d("CodeStyleGood", "A we really using this method?");
        }
        if (isThisBetterMethod()) {
            sStaticField = mPrivateField;
            mPrivateField = sStaticField;
            checkIndentation();
        }
    }
}

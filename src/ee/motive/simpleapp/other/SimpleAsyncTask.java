
package ee.motive.simpleapp.other;

import android.os.AsyncTask;
import android.os.SystemClock;
import android.widget.ProgressBar;
import android.widget.TextView;

import ee.motive.simpleapp.Config;
import ee.motive.simpleapp.util.Logger;

/**
 * Example AsyncTask class.
 */
public class SimpleAsyncTask extends AsyncTask<String, Integer, String> {

    private final String TAG = Logger.TAG_PREFIX + this.getClass().getSimpleName();
    private static final Logger logger = Logger.getInstance();
    private static final boolean LOCAL_LOG = Config.DEBUG;

    private TextView textView;
    private ProgressBar progressBar;

    public SimpleAsyncTask(TextView textView, ProgressBar progressBar) {
        this.textView = textView;
        this.textView.setText("simpleAsyncTask initialize");
        this.progressBar = progressBar;
        this.progressBar.setProgress(0);
    }

    /** Invoked on the UI thread immediately after the task is executed. */
    @Override
    protected void onPreExecute() {
        if (LOCAL_LOG) {
            logger.d(TAG, "simpleAsyncTask onPreExecute()");
        }
        textView.setText("simpleAsyncTask onPreExecute()");
    }

    /**
     * Invoked on the background thread immediately after onPreExecute method.
     * 
     * @param params A string array of parameters.
     * @return Value to onPostExecute method.
     */
    @Override
    protected String doInBackground(String... params) {
        if (LOCAL_LOG) {
            logger.dBackground(TAG, "simpleAsyncTask doInBackground(" + params[0] + ")");
            logger.dBackground(TAG, "simpleAsyncTask priority: " +
                    android.os.Process.getThreadPriority(android.os.Process.myTid()));
        }
        // Do some work.
        for (int i = 0; i < 100; i++) {
            // Check for interruption.
            if (!isCancelled()) {
                // Sleep for a while.
                // Could also use Thread.sleep(100);
                // and catch InterruptedException.
                SystemClock.sleep(100);
                // After a long running process,
                // check for interruption, again.
                // Invoke the onProgressUpdate method with current progress.
                if (!isCancelled()) {
                    publishProgress(i);
                }
            } else { // If thread is cancelled, end the task.
                // Could also use a Handler.
                if (LOCAL_LOG) {
                    logger.dBackground(TAG, "simpleAsyncTask interrupted");
                }
                textView.post(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText("simpleAsyncTask interrupted");
                        progressBar.setProgress(0);
                    }
                });
                return "not finished";
            }
        }
        return "finished";
    }

    /**
     * Invoked on the UI thread when publishProgress is called.
     * 
     * @param progress Current progress.
     */
    @Override
    protected void onProgressUpdate(Integer... progress) {
        if (LOCAL_LOG) {
            logger.d(TAG, "simpleAsyncTask onProgressUpdate(" + progress[0] + ")");
        }
        textView.setText("simpleAsyncTask progress: " + progress[0]);
        progressBar.setProgress(progress[0]);
    }

    /**
     * Invoked on the UI thread after the background thread finishes.
     * 
     * @param result Value from doInBackground method.
     */
    @Override
    protected void onPostExecute(String result) {
        if (LOCAL_LOG) {
            logger.d(TAG, "simpleAsyncTask onPostExecute(" + result + ")");
        }
        textView.setText("simpleAsyncTask finished");
    }

}


package ee.motive.simpleapp.other;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Android Annotations SharedPreferences helper class.
 */
@SharedPref
public interface SimplePreferencesHelper {

    // The field isSimpleAsyncTaskRunning has a default value false.
    @DefaultBoolean(false)
    boolean isSimpleAsyncTaskRunning();

    // The field isSimpleThreadRunning has a default value false.
    @DefaultBoolean(false)
    boolean isSimpleThreadRunning();
}


package ee.motive.simpleapp;

/**
 * Application configuration class to hold constants.
 */
public class Config {
    /**
     * If true, then application will send log messages.<br>
     * If false, then all debug messages will be compiled out.
     */
    public static final boolean DEBUG = true;
}

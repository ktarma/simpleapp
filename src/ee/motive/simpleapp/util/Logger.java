
package ee.motive.simpleapp.util;

import android.util.Log;
import android.widget.TextView;

import ee.motive.simpleapp.Config;

/**
 * A simple singleton logging class. This is the eager approach.<br>
 */
public class Logger {

    public static final String TAG_PREFIX = "Tag_";
    private final String CLASS_NAME = this.getClass().getSimpleName();
    private final String TAG = TAG_PREFIX + CLASS_NAME;
    private static final Logger instance = new Logger();
    private static final boolean LOCAL_LOG = Config.DEBUG;

    private static final StringBuilder stringBuilder = new StringBuilder("");
    private static TextView logView = null;
    private static final String newRow = "\n";

    /** Constructor, hidden because this is a singleton. */
    private Logger() {

    }

    /**
     * Getting the singleton instance.
     * 
     * @return Logger instance.
     */
    public static Logger getInstance() {
        return instance; // Return the instance.
    }

    /**
     * Setting the logging TextView.
     * 
     * @param logView TextView that logs. To unset, pass null.
     */
    public void setLogView(TextView logView) {
        Logger.logView = logView;
    }

    /**
     * Sending a DEBUG log message from the UI thread.
     * 
     * @param tag Used to identify the source of a log message. It usually
     *            identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public void d(String tag, String msg) {
        if (LOCAL_LOG) {
            Log.d(tag, msg);
        }
        stringBuilder.append(msg).append(newRow);
        setTextAndScroll();
    }

    /**
     * Sending a DEBUG log message from a background thread.
     * 
     * @param tag Used to identify the source of a log message. It usually
     *            identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public void dBackground(String tag, String msg) {
        if (LOCAL_LOG) {
            Log.d(tag, msg);
        }
        stringBuilder.append(msg).append(newRow);
        if (logView != null) {
            logView.post(new Runnable() {
                @Override
                public void run() {
                    setTextAndScroll();
                }
            });
        }
    }

    private void setTextAndScroll() {
        if (logView != null) {
            logView.setText(stringBuilder.toString());
            // Scrolling the TextView to see the last row.
            int y = logView.getLineHeight() * logView.getLineCount()
                    - (logView.getBottom() - logView.getTop());
            if (y > 0) {
                logView.scrollTo(0, y);
            } else {
                logView.scrollTo(0, 0);
            }
        }
    }

    /** Clearing the log. */
    public void clear() {
        stringBuilder.setLength(0);
        setTextAndScroll();
    }
}

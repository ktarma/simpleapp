
package ee.motive.simpleapp.model;

import ee.motive.simpleapp.Config;
import ee.motive.simpleapp.util.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple model object. Data may be saved to or loaded from the Shared
 * Preferences, internal or external storage as files, databases or the web.
 */
public class SimpleData {

    private final String CLASS_NAME = this.getClass().getSimpleName();
    private final String TAG = Logger.TAG_PREFIX + CLASS_NAME;
    private static final Logger logger = Logger.getInstance();
    private static final boolean LOCAL_LOG = Config.DEBUG;

    /**
     * A list of elements. Alternatively, it may also be a HashMap.<br>
     * Could implement Serializable or Parcelable, but not in current scope.
     */
    private List<SimpleElement> elements = new ArrayList<SimpleElement>();

    /** Constructor. */
    public SimpleData() {
        initialize();
    }

    /** Initializing model and its data. */
    private void initialize() {
        addElement(new SimpleElement("1", "First"));
        addElement(new SimpleElement("2", "Second"));
        addElement(new SimpleElement("3", "Third"));
    }

    private void addElement(SimpleElement element) {
        elements.add(element);
    }

    /**
     * Getting an element by id.
     * 
     * @param id The element id.
     * @return The element with the specified id or null, if there is no
     *         matching element.
     */
    public SimpleElement getElementById(String id) {
        for (SimpleElement element : elements) {
            if (element.getId().equals(id)) {
                return element;
            }
        }
        return null;
    }

    /**
     * Getting a list of all the elements.
     * 
     * @return A list of all the elements.
     */
    public List<SimpleElement> getElements() {
        return elements;
    }

    public void logElements() {
        for (SimpleElement item : elements) {
            if (LOCAL_LOG) {
                logger.d(TAG, "SimpleData has item: " + item.getId() + ", " + item.getContent());
            }
        }
    }
}

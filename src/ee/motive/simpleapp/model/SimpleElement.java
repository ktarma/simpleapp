
package ee.motive.simpleapp.model;

/**
 * An element representing a piece of data.<br>
 * Could implement Serializable or Parcelable, but not in current scope.
 */
public class SimpleElement {

    private String id;
    private String content;

    public SimpleElement() {

    }

    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public SimpleElement(String id, String content) {
        this.id = id;
        this.content = content;
    }
}

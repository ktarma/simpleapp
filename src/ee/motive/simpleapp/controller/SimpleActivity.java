
package ee.motive.simpleapp.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.Time;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import ee.motive.simpleapp.Config;
import ee.motive.simpleapp.R;
import ee.motive.simpleapp.SimpleApp;
import ee.motive.simpleapp.model.SimpleData;
import ee.motive.simpleapp.other.SimpleAsyncTask;
import ee.motive.simpleapp.util.Logger;

/**
 * Example Activity class to demonstrate:<br>
 * 1. A string that saves state.<br>
 * 2. Saving and loading preferences (simple info).<br>
 * 3. Starting and stopping a Service.<br>
 * 4. Initializing a simple data object.<br>
 * 5. Starting a BroadcastReceiver.<br>
 * 6. Starting and stopping an AsyncTask.<br>
 * 7. Starting, pausing, resuming and stopping a Java Thread.<br>
 * 8. Logging.<br>
 */
public class SimpleActivity extends Activity {

    private final String CLASS_NAME = this.getClass().getSimpleName();
    private final String TAG = Logger.TAG_PREFIX + CLASS_NAME;
    private static final Logger logger = Logger.getInstance();
    private static final boolean LOCAL_LOG = Config.DEBUG;

    // State of this string is saved and loaded by methods:
    // onSaveInstanceState, onRestoreInstanceState and onCreate.
    // To save and load the state we use a final key "SAVEABLE_STRING_KEY".
    private String stateString;
    private static final String SAVEABLE_STRING_KEY = "SAVEABLE_STRING_KEY";

    // Preferences are saved using application wise SharedPreferences in
    // SimpleApp class.
    // Preference values need a key:
    private static final String SIMPLE_ASYNC_TASK_RUNNING = "SIMPLE_ASYNC_TASK_RUNNING_KEY";
    private static final String SIMPLE_THREAD_RUNNING = "SIMPLE_THREAD_RUNNING_KEY";

    private Intent simpleService;
    private SimpleData simpleData;
    private AsyncTask<String, Integer, String> simpleAsyncTask;
    private Thread simpleThread;
    private Object simpleThreadLock = new Object();
    private boolean isSimpleThreadPaused = false;

    private Button serviceStart;
    private Button serviceStop;
    private Button dataStart;
    private Button broadcastReceiverStart;
    private TextView asyncTaskText;
    private Button asyncTaskStart;
    private Button asyncTaskStop;
    private ProgressBar asyncTaskProgressBar;
    private TextView threadText;
    private Button threadStart;
    private Button threadStop;
    private Button threadPause;
    private Button threadResume;
    private ProgressBar threadProgressBar;
    private TextView logView;
    private Button logClear;

    /* LIFECYCLE METHODS: */
    // onCreate()
    // onRestart()
    // onStart()
    // onRestoreInstanceState()
    // onResume()
    // onPause()
    // onSaveInstanceState()
    // onStop()
    // onDestroy()

    // Called when activity is first created.
    // Also gets the last saved state from a bundle.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            // Restoring the stateString from a bundle.
            stateString = savedInstanceState.getString(SAVEABLE_STRING_KEY);
        }
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onCreate, stateString: " + stateString);
        }

        setContentView(R.layout.activity_simple);

        serviceStart = (Button) findViewById(R.id.service_start);
        serviceStart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startSimpleService();
            }
        });

        serviceStop = (Button) findViewById(R.id.service_stop);
        serviceStop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stopSimpleService();
            }
        });

        dataStart = (Button) findViewById(R.id.data_start);
        dataStart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startSimpleData();
            }
        });

        broadcastReceiverStart = (Button) findViewById(R.id.broadcast_receiver_start);
        broadcastReceiverStart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startSimpleBroadcastReceiver();
            }
        });

        asyncTaskText = (TextView) findViewById(R.id.async_task_text);
        asyncTaskText.setText("simpleAsyncTask not started");
        asyncTaskStart = (Button) findViewById(R.id.async_task_start);
        asyncTaskStart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startSimpleAsyncTask();
            }
        });
        asyncTaskStop = (Button) findViewById(R.id.async_task_stop);
        asyncTaskStop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stopSimpleAsyncTask();
            }
        });
        asyncTaskProgressBar = (ProgressBar) findViewById(R.id.async_task_progress);

        threadText = (TextView) findViewById(R.id.thread_text);
        threadText.setText("simpleThread not started");
        threadStart = (Button) findViewById(R.id.thread_start);
        threadStart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startSimpleThread();
            }
        });
        threadStop = (Button) findViewById(R.id.thread_stop);
        threadStop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stopSimpleThread();
            }
        });
        threadPause = (Button) findViewById(R.id.thread_pause);
        threadPause.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseSimpleThread();
            }
        });
        threadResume = (Button) findViewById(R.id.thread_resume);
        threadResume.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resumeSimpleThread();
            }
        });
        threadProgressBar = (ProgressBar) findViewById(R.id.thread_progress);

        logView = (TextView) findViewById(R.id.log_view);
        logView.setMovementMethod(new ScrollingMovementMethod());
        logClear = (Button) findViewById(R.id.log_clear);
        logClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                logger.clear();
            }
        });
        logger.setLogView(logView);
    }

    // Called when activity is about to become visible, before onStart(),
    // but only if onStop() has already been called.
    @Override
    protected void onRestart() {
        super.onRestart();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onRestart");
        }
    }

    // Called when activity is about to become visible.
    @Override
    protected void onStart() {
        super.onStart();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onStart");
        }
    }

    // Called after onStart(),
    // but only if the activity is being re-initialized from a previously saved
    // state, like after a configuration change.
    // Gets the last saved state from a bundle.
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restoring the stateString from a bundle.
        stateString = savedInstanceState.getString(SAVEABLE_STRING_KEY);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onRestoreInstanceState, stateString: " + stateString);
        }
    }

    // Called when activity has become visible.
    @Override
    protected void onResume() {
        super.onResume();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onResume");
        }
        // Setting the stateString to a new value (current time).
        Time time = new Time();
        time.setToNow();
        stateString = time.format("%H:%M:%S");

        // Load flags for previously running tasks from preferences
        // and restart them.
        SimpleApp simpleApp = (SimpleApp) getApplicationContext();
        if (simpleApp.getBoolean(SIMPLE_ASYNC_TASK_RUNNING, false)) {
            startSimpleAsyncTask();
        }
        if (simpleApp.getBoolean(SIMPLE_THREAD_RUNNING, false)) {
            startSimpleThread();
        }
    }

    // Called when activity is partially visible.
    @Override
    protected void onPause() {
        super.onPause();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onPause");
        }

        // Save flags for currently running tasks to preferences.
        SimpleApp simpleApp = (SimpleApp) getApplicationContext();
        if (simpleAsyncTask != null && simpleAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            simpleApp.setBoolean(SIMPLE_ASYNC_TASK_RUNNING, true);
        } else {
            simpleApp.setBoolean(SIMPLE_ASYNC_TASK_RUNNING, false);
        }
        if (simpleThread != null) {
            simpleApp.setBoolean(SIMPLE_THREAD_RUNNING, true);
        } else {
            simpleApp.setBoolean(SIMPLE_THREAD_RUNNING, false);
        }

        // Stop currently running tasks.
        stopSimpleAsyncTask();
        stopSimpleThread();
    }

    // Called before onStop(),
    // but only if activity is closed by home button or another activity becomes
    // visible.
    // This method does not work if activity is closed by back button!
    // Save the activity state to a bundle.
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Saving the stateString to a bundle.
        outState.putString(SAVEABLE_STRING_KEY, stateString);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onSaveInstanceState, stateString: " + stateString);
        }
    }

    // Called when activity is no longer visible.
    @Override
    protected void onStop() {
        super.onStop();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onStop");
        }
    }

    // Called just before activity is destroyed.
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onDestroy");
        }
        logger.setLogView(null);
    }

    /* CUSTOM METHODS */

    private void startSimpleData() {
        if (LOCAL_LOG) {
            logger.d(TAG, "startSimpleData");
        }
        if (simpleData == null) {
            simpleData = new SimpleData();
        }
        simpleData.logElements();
    }

    private void startSimpleService() {
        if (LOCAL_LOG) {
            logger.d(TAG, "startSimpleService");
        }
        simpleService = new Intent(this, SimpleService.class);
        simpleService.putExtra(SimpleService.EXTRA, "Value to service");
        startService(simpleService);
    }

    private void stopSimpleService() {
        if (simpleService != null) {
            stopService(simpleService);
        }
    }

    private void startSimpleBroadcastReceiver() {
        if (LOCAL_LOG) {
            logger.d(TAG, "startSimpleBroadcastReceiver");
        }
        Intent simpleBroadcastReceiver = new Intent(this, SimpleBroadcastReceiver.class);
        simpleBroadcastReceiver.putExtra(SimpleBroadcastReceiver.EXTRA, "Value to receiver");
        sendBroadcast(simpleBroadcastReceiver);
    }

    private void startSimpleAsyncTask() {
        if (simpleAsyncTask == null || simpleAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
            if (LOCAL_LOG) {
                logger.d(TAG, "simpleAsyncTask startSimpleAsyncTask");
            }
            simpleAsyncTask = new SimpleAsyncTask(asyncTaskText, asyncTaskProgressBar);
            simpleAsyncTask.execute("some params");
        }
    }

    private void stopSimpleAsyncTask() {
        if (simpleAsyncTask != null && simpleAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            if (LOCAL_LOG) {
                logger.d(TAG, "simpleAsyncTask stopSimpleAsyncTask");
            }
            simpleAsyncTask.cancel(true);
            simpleAsyncTask = null;
        }
    }

    private void startSimpleThread() {
        if (simpleThread == null) {
            if (LOCAL_LOG) {
                logger.d(TAG, "simpleThread startSimpleThread");
            }
            isSimpleThreadPaused = false;
            simpleThread = new Thread(new SimpleThreadRunnable());
            simpleThread.start();
        }
    }

    public class SimpleThreadRunnable implements Runnable {
        @Override
        public void run() {
            if (LOCAL_LOG) {
                logger.dBackground(TAG, "simpleThread priority: " +
                        android.os.Process.getThreadPriority(android.os.Process.myTid()));
                // Could set the new priority:
                // android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            }
            // Do some work.
            for (int i = 0; i < 100; i++) {
                // Check if thread should be paused.
                synchronized (simpleThreadLock) {
                    try {
                        if (isSimpleThreadPaused) {
                            // Wait here until notify or notifyAll is called.
                            simpleThreadLock.wait();
                        }
                    } catch (InterruptedException e) {
                        // Pause was interrupted.
                        break;
                    }
                }
                // Check for interruption.
                if (!Thread.currentThread().isInterrupted()) {
                    // Sleep for a while.
                    // Could also use Thread.sleep(100);
                    // and catch InterruptedException.
                    SystemClock.sleep(100);
                    // After a long running process,
                    // check for interruption, again.
                    if (!Thread.currentThread().isInterrupted()) {
                        // Set the current progress.
                        final int progress = i;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (LOCAL_LOG) {
                                    logger.d(TAG, "simpleThread progress: " + progress);
                                }
                                threadText.setText("simpleThread progress: " + progress);
                                threadProgressBar.setProgress(progress);
                            }
                        });
                    }
                } else {
                    break;
                }
            }
            // The work is done OR it was interrupted.
            final boolean isInterrupted = Thread.currentThread().isInterrupted()
                    || isSimpleThreadPaused;
            if (LOCAL_LOG) {
                if (isInterrupted) {
                    logger.dBackground(TAG, "simpleThread interrupted");
                } else {
                    logger.dBackground(TAG, "simpleThread finished");
                }
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isInterrupted) {
                        threadText.setText("simpleThread interrupted");
                        threadProgressBar.setProgress(0);
                    } else {
                        threadText.setText("simpleThread finished");
                    }
                    simpleThread = null;
                }
            });
        }
    }

    private void pauseSimpleThread() {
        if (LOCAL_LOG) {
            logger.d(TAG, "simpleThread pauseSimpleThread");
        }
        synchronized (simpleThreadLock) {
            isSimpleThreadPaused = true;
        }
    }

    private void resumeSimpleThread() {
        if (LOCAL_LOG) {
            logger.d(TAG, "simpleThread resumeSimpleThread");
        }
        synchronized (simpleThreadLock) {
            isSimpleThreadPaused = false;
            simpleThreadLock.notifyAll();
        }
    }

    private void stopSimpleThread() {
        if (simpleThread != null) {
            if (LOCAL_LOG) {
                logger.d(TAG, "simpleThread stopSimpleThread");
            }
            simpleThread.interrupt();
        }
    }
}


package ee.motive.simpleapp.controller;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import ee.motive.simpleapp.R;
import ee.motive.simpleapp.util.Logger;

/**
 * A simple FragmentActivity to demonstrate XML view, Java view and a fragment.
 */
public class MainFragmentActivity extends FragmentActivity {

    private TextView logView;
    private Button logClear;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity_main);

        logView = (TextView) findViewById(R.id.log_view);
        logView.setMovementMethod(new ScrollingMovementMethod());
        logClear = (Button) findViewById(R.id.log_clear);
        logClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.getInstance().clear();
            }
        });
        Logger.getInstance().setLogView(logView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.getInstance().setLogView(null);
    }
}

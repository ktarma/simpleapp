
package ee.motive.simpleapp.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ee.motive.simpleapp.Config;
import ee.motive.simpleapp.R;
import ee.motive.simpleapp.util.Logger;

/**
 * A simple Fragment class to use in XML.
 */
public class SimpleFragment extends Fragment {

    private final String CLASS_NAME = this.getClass().getSimpleName();
    private final String TAG = Logger.TAG_PREFIX + CLASS_NAME;
    private static final Logger logger = Logger.getInstance();
    private static final boolean LOCAL_LOG = Config.DEBUG;

    /* LIFECYCLE METHODS: */
    // onAttach()
    // onCreate()
    // onCreateView()
    // onActivityCreated()
    // onStart()
    // onResume()
    // onPause()
    // onStop()
    // onDestroyView()
    // onDestroy()
    // onDetach()

    // Called once the fragment is associated with its activity.
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onAttach");
        }
    }

    // Called to do initial creation of the fragment.
    @Override
    public void onCreate(Bundle saved) {
        super.onCreate(saved);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onCreate");
        }
    }

    // Called when it's time for the fragment to draw its user interface for the
    // first time. To draw a UI for your fragment, you must return a View from
    // this method that is the root of your fragment's layout.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onCreateView");
        }
        // Inflate the layout for this fragment.
        View view = inflater.inflate(R.layout.fragment_simple, container, false);
        TextView textView = (TextView) view.findViewById(R.id.text_view);
        textView.setText("This is a fragment.");
        return view;
    }

    // Tells the fragment that its activity has completed its own
    // Activity.onCreate().
    @Override
    public void onActivityCreated(Bundle saved) {
        super.onActivityCreated(saved);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onActivityCreated");
        }
    }

    // Makes the fragment visible to the user (based on its containing activity
    // being started).
    @Override
    public void onStart() {
        super.onStart();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onStart");
        }
    }

    // Makes the fragment interacting with the user (based on its containing
    // activity being resumed).
    @Override
    public void onResume() {
        super.onResume();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onResume");
        }
    }

    // Fragment is no longer interacting with the user either because its
    // activity is being paused or a fragment operation is modifying it in the
    // activity.
    @Override
    public void onPause() {
        super.onPause();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onPause");
        }
    }

    // Fragment is no longer visible to the user either because its activity is
    // being stopped or a fragment operation is modifying it in the activity.
    @Override
    public void onStop() {
        super.onStop();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onStop");
        }
    }

    // Allows the fragment to clean up resources associated with its View.
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onDestroyView");
        }
    }

    // Called to do final cleanup of the fragment's state.
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onDestroy");
        }
    }

    // Called immediately prior to the fragment no longer being associated with
    // its activity.
    @Override
    public void onDetach() {
        super.onDetach();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onDetach");
        }
    }
}

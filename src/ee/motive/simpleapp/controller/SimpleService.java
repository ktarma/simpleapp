
package ee.motive.simpleapp.controller;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import ee.motive.simpleapp.Config;
import ee.motive.simpleapp.util.Logger;

/**
 * Example Service class.
 */
public class SimpleService extends Service {

    private final String CLASS_NAME = this.getClass().getSimpleName();
    private final String TAG = Logger.TAG_PREFIX + CLASS_NAME;
    private static final Logger logger = Logger.getInstance();
    private static final boolean LOCAL_LOG = Config.DEBUG;

    public static final String EXTRA = "SERVICE_EXTRA";

    // The service is being created.
    @Override
    public void onCreate() {
        super.onCreate();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onCreate");
        }
    }

    // The service is starting, due to a call to startService().
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onStartCommand");
        }
        String stringExtra = intent.getStringExtra(EXTRA);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " " + EXTRA + ": " + stringExtra);
        }
        return Service.START_NOT_STICKY;
    }

    // A client is binding to the service with bindService().
    @Override
    public IBinder onBind(Intent intent) {
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onBind");
        }
        return null;
    }

    // All clients have unbound with unbindService().
    @Override
    public boolean onUnbind(Intent intent) {
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onUnbind");
        }
        return false;
    }

    // A client is binding to the service with bindService(),
    // after onUnbind() has already been called.
    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onRebind");
        }
    }

    // The service is no longer used and is being destroyed.
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onDestroy");
        }
    }
}

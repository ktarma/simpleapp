
package ee.motive.simpleapp.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ee.motive.simpleapp.Config;
import ee.motive.simpleapp.util.Logger;

/**
 * Example BroadcastReceiver class.
 */
public class SimpleBroadcastReceiver extends BroadcastReceiver {

    private final String CLASS_NAME = this.getClass().getSimpleName();
    private final String TAG = Logger.TAG_PREFIX + CLASS_NAME;
    private static final Logger logger = Logger.getInstance();
    private static final boolean LOCAL_LOG = Config.DEBUG;

    public static final String EXTRA = "RECEIVER_EXTRA";

    // The broadcast receiver received an intent.
    @Override
    public void onReceive(Context context, Intent intent) {
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onReceive");
        }
        String stringExtra = intent.getStringExtra(EXTRA);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " " + EXTRA + ": " + stringExtra);
        }
    }
}

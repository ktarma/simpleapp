
package ee.motive.simpleapp.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.Time;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import ee.motive.simpleapp.Config;
import ee.motive.simpleapp.R;
import ee.motive.simpleapp.model.SimpleData;
import ee.motive.simpleapp.other.SimpleAsyncTask;
import ee.motive.simpleapp.other.SimplePreferencesHelper_;
import ee.motive.simpleapp.util.Logger;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.api.BackgroundExecutor;

/**
 * Example Activity class (annotated using Android Annotations) to demonstrate:<br>
 * 0. Usage of different AA annotations.<br>
 * 1. A string that saves state (with AA).<br>
 * 2. Saving and loading preferences (simple info) (with AA).<br>
 * 3. Starting and stopping a Service.<br>
 * 4. Initializing a simple data object.<br>
 * 5. Starting a BroadcastReceiver.<br>
 * 6. Starting and stopping an AsyncTask.<br>
 * 7. Starting, pausing, resuming and stopping a Java Thread (with AA).<br>
 * 8. Logging.<br>
 */
@EActivity(R.layout.activity_simple)
public class SimpleActivityAnnotated extends Activity {

    private final String CLASS_NAME = this.getClass().getSimpleName();
    private final String TAG = Logger.TAG_PREFIX + CLASS_NAME;
    private static final Logger logger = Logger.getInstance();
    private static final boolean LOCAL_LOG = Config.DEBUG;

    // State of this string is saved and loaded by Android Annotations.
    @InstanceState
    String stateString;

    // Preferences are saved by using Android Annotations methods and
    // SimplePreferencesHelper class.
    // This way preference values don't need a key.
    @Pref
    SimplePreferencesHelper_ preferences;

    private Intent simpleService;
    private SimpleData simpleData;
    private AsyncTask<String, Integer, String> simpleAsyncTask;
    private boolean isSimpleThreadRunning = false;
    private Object simpleThreadLock = new Object();
    private boolean isSimpleThreadPaused = false;

    @ViewById(R.id.service_start)
    Button serviceStart;
    @ViewById(R.id.service_stop)
    Button serviceStop;
    @ViewById(R.id.data_start)
    Button dataStart;
    @ViewById(R.id.broadcast_receiver_start)
    Button broadcastReceiverStart;
    @ViewById(R.id.async_task_text)
    TextView asyncTaskText;
    @ViewById(R.id.async_task_start)
    Button asyncTaskStart;
    @ViewById(R.id.async_task_stop)
    Button asyncTaskStop;
    @ViewById(R.id.async_task_progress)
    ProgressBar asyncTaskProgressBar;
    @ViewById(R.id.thread_text)
    TextView threadText;
    @ViewById(R.id.thread_start)
    Button threadStart;
    @ViewById(R.id.thread_stop)
    Button threadStop;
    @ViewById(R.id.thread_progress)
    ProgressBar threadProgressBar;
    @ViewById(R.id.log_view)
    TextView logView;
    @ViewById(R.id.log_clear)
    Button logClear;

    /* LIFECYCLE METHODS: */
    // onCreate()
    // afterViews()
    // onRestart()
    // onStart()
    // onRestoreInstanceState()
    // onResume()
    // onPause()
    // onSaveInstanceState()
    // onStop()
    // onDestroy()

    // Called when activity is first created.
    // Also gets the last saved state from a bundle.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onCreate, stateString: " + stateString);
        }
    }

    // Called after onCreate(), views are now binded.
    @AfterViews
    void afterViews() {
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " afterViews");
        }
        asyncTaskText.setText("simpleAsyncTask not started");
        threadText.setText("simpleThread not started");
        logView.setMovementMethod(new ScrollingMovementMethod());
        logger.setLogView(logView);
    }

    // Called when activity is about to become visible, before onStart(),
    // but only if onStop() has already been called.
    @Override
    protected void onRestart() {
        super.onRestart();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onRestart");
        }
    }

    // Called when activity is about to become visible.
    @Override
    protected void onStart() {
        super.onStart();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onStart");
        }
    }

    // Called after onStart(),
    // but only if the activity is being re-initialized from a previously saved
    // state, like after a configuration change.
    // Gets the last saved state from a bundle.
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onRestoreInstanceState, stateString: " + stateString);
        }
    }

    // Called when activity has become visible.
    @Override
    protected void onResume() {
        super.onResume();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onResume");
        }
        // Setting the stateString to a new value (current time).
        Time time = new Time();
        time.setToNow();
        stateString = time.format("%H:%M:%S");

        // Load flags for previously running tasks from preferences
        // and restart them.
        if (preferences.isSimpleAsyncTaskRunning().get()) {
            startSimpleAsyncTask();
        }
        if (preferences.isSimpleThreadRunning().get()) {
            startSimpleThread();
        }
    }

    // Called when activity is partially visible.
    @Override
    protected void onPause() {
        super.onPause();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onPause");
        }

        // Save flags for currently running tasks to preferences.
        if (simpleAsyncTask != null && simpleAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            preferences.isSimpleAsyncTaskRunning().put(true);
        } else {
            preferences.isSimpleAsyncTaskRunning().put(false);
        }
        if (isSimpleThreadRunning) {
            preferences.isSimpleThreadRunning().put(true);
        } else {
            preferences.isSimpleThreadRunning().put(false);
        }

        // Stop currently running tasks.
        stopSimpleAsyncTask();
        stopSimpleThread();
    }

    // Called before onStop(),
    // but only if activity is closed by home button or another activity becomes
    // visible.
    // This method does not work if activity is closed by back button!
    // Save the activity state to a bundle.
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onSaveInstanceState, stateString: " + stateString);
        }
    }

    // Called when activity is no longer visible.
    @Override
    protected void onStop() {
        super.onStop();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onStop");
        }
    }

    // Called just before activity is destroyed.
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onDestroy");
        }
        logger.setLogView(null);
    }

    /* CUSTOM METHODS */

    @Click(R.id.data_start)
    void startSimpleData() {
        if (LOCAL_LOG) {
            logger.d(TAG, "startSimpleData");
        }
        if (simpleData == null) {
            simpleData = new SimpleData();
        }
        simpleData.logElements();
    }

    @Click(R.id.service_start)
    void startSimpleService() {
        if (LOCAL_LOG) {
            logger.d(TAG, "startSimpleService");
        }
        simpleService = new Intent(this, SimpleService.class);
        simpleService.putExtra(SimpleService.EXTRA, "Value to service");
        startService(simpleService);
    }

    @Click(R.id.service_stop)
    void stopSimpleService() {
        if (simpleService != null) {
            stopService(simpleService);
        }
    }

    @Click(R.id.broadcast_receiver_start)
    void startSimpleBroadcastReceiver() {
        if (LOCAL_LOG) {
            logger.d(TAG, "startSimpleBroadcastReceiver");
        }
        Intent simpleBroadcastReceiver = new Intent(this, SimpleBroadcastReceiver.class);
        simpleBroadcastReceiver.putExtra(SimpleBroadcastReceiver.EXTRA, "Value to receiver");
        sendBroadcast(simpleBroadcastReceiver);
    }

    @Click(R.id.async_task_start)
    void startSimpleAsyncTask() {
        if (simpleAsyncTask == null || simpleAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
            if (LOCAL_LOG) {
                logger.d(TAG, "simpleAsyncTask startSimpleAsyncTask");
            }
            simpleAsyncTask = new SimpleAsyncTask(asyncTaskText, asyncTaskProgressBar);
            simpleAsyncTask.execute("some params");
        }
    }

    @Click(R.id.async_task_stop)
    void stopSimpleAsyncTask() {
        if (simpleAsyncTask != null && simpleAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            if (LOCAL_LOG) {
                logger.d(TAG, "simpleAsyncTask stopSimpleAsyncTask");
            }
            simpleAsyncTask.cancel(true);
            simpleAsyncTask = null;
        }
    }

    @Click(R.id.thread_start)
    void startSimpleThread() {
        if (isSimpleThreadRunning == false) {
            if (LOCAL_LOG) {
                logger.d(TAG, "simpleThread startSimpleThread");
            }
            isSimpleThreadPaused = false;
            isSimpleThreadRunning = true;
            simpleThreadRunnable();
        }
    }

    @Background(id = "simpleThreadId")
    void simpleThreadRunnable() {
        if (LOCAL_LOG) {
            logger.dBackground(TAG, "simpleThread priority: " +
                    android.os.Process.getThreadPriority(android.os.Process.myTid()));
        }
        // Do some work.
        for (int i = 0; i < 100; i++) {
            // Check if thread should be paused.
            synchronized (simpleThreadLock) {
                try {
                    if (isSimpleThreadPaused) {
                        // Wait here until notify or notifyAll is called.
                        simpleThreadLock.wait();
                    }
                } catch (InterruptedException e) {
                    // Pause was interrupted.
                    break;
                }
            }
            // Check for interruption.
            if (!Thread.currentThread().isInterrupted()) {
                // Sleep for a while.
                // Could also use Thread.sleep(100);
                // and catch InterruptedException.
                SystemClock.sleep(100);
                // After a long running process,
                // check for interruption, again.
                if (!Thread.currentThread().isInterrupted()) {
                    // Set the current progress.
                    setProgressSimpleThread(i);
                }
            } else {
                break;
            }
        }
        // The work is done OR it was interrupted.
        final boolean isInterrupted = Thread.currentThread().isInterrupted()
                || isSimpleThreadPaused;
        if (LOCAL_LOG) {
            if (isInterrupted) {
                logger.dBackground(TAG, "simpleThread interrupted");
            } else {
                logger.dBackground(TAG, "simpleThread finished");
            }
        }
        finishedSimpleThread(isInterrupted);
    }

    @UiThread
    void setProgressSimpleThread(int progress) {
        if (LOCAL_LOG) {
            logger.d(TAG, "simpleThread progress: " + progress);
        }
        threadText.setText("simpleThread progress: " + progress);
        threadProgressBar.setProgress(progress);
    }

    @UiThread
    void finishedSimpleThread(boolean isInterrupted) {
        if (isInterrupted) {
            threadText.setText("simpleThread interrupted");
            threadProgressBar.setProgress(0);
        } else {
            threadText.setText("simpleThread finished");
        }
        isSimpleThreadRunning = false;
    }

    @Click(R.id.thread_pause)
    void pauseSimpleThread() {
        if (LOCAL_LOG) {
            logger.d(TAG, "simpleThread pauseSimpleThread");
        }
        synchronized (simpleThreadLock) {
            isSimpleThreadPaused = true;
        }
    }

    @Click(R.id.thread_resume)
    void resumeSimpleThread() {
        if (LOCAL_LOG) {
            logger.d(TAG, "simpleThread resumeSimpleThread");
        }
        synchronized (simpleThreadLock) {
            isSimpleThreadPaused = false;
            simpleThreadLock.notifyAll();
        }
    }

    @Click(R.id.thread_stop)
    void stopSimpleThread() {
        if (isSimpleThreadRunning) {
            if (LOCAL_LOG) {
                logger.d(TAG, "simpleThread stopSimpleThread");
            }
            BackgroundExecutor.cancelAll("simpleThreadId", true);
        }
    }

    @Click(R.id.log_clear)
    void clearLog() {
        logger.clear();
    }
}

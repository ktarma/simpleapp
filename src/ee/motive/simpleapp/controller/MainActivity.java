
package ee.motive.simpleapp.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import ee.motive.simpleapp.R;

/**
 * A simple Activity to list some activities.
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Context context = this;

        final ListView listView = (ListView) findViewById(R.id.list_view);
        String[] values = new String[] {
                "SimpleActivity", "SimpleActivityAnnotated_", "MainFragmentActivity"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String itemValue = (String) listView.getItemAtPosition(position);
                Intent activityIntent = null;
                if ("SimpleActivity".equals(itemValue)) {
                    activityIntent = new Intent(context, SimpleActivity.class);
                } else if ("SimpleActivityAnnotated_".equals(itemValue)) {
                    activityIntent = new Intent(context, SimpleActivityAnnotated_.class);
                } else if ("MainFragmentActivity".equals(itemValue)) {
                    activityIntent = new Intent(context, MainFragmentActivity.class);
                }
                if (activityIntent != null) {
                    context.startActivity(activityIntent);
                }
            }
        });
    }
}


package ee.motive.simpleapp;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import ee.motive.simpleapp.util.Logger;

/**
 * Base class for those who need to maintain global application state.
 */
public class SimpleApp extends Application {

    private final String CLASS_NAME = this.getClass().getSimpleName();
    private final String TAG = Logger.TAG_PREFIX + CLASS_NAME;
    private static final Logger logger = Logger.getInstance();
    private static final boolean LOCAL_LOG = Config.DEBUG;

    private static SimpleApp instance;

    /**
     * Returns a singleton Application instance.<br>
     * This method could be used to get the Application without a context:<br>
     * SimpleApp.getInstance().setBoolean("IT_WORKS", true);
     * 
     * @return SimpleApp instance.
     */
    public static SimpleApp getInstance() {
        return instance;
    }

    // Application wise preferences to save simple info.
    private SharedPreferences preferences;
    private SharedPreferences.Editor preferencesEditor;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onCreate");
        }
        // Set application wise SharedPreferences and SharedPreferences.Editor.
        preferences = getApplicationContext().getSharedPreferences("PREFERENCES_KEY", MODE_PRIVATE);
        preferencesEditor = preferences.edit();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onConfigurationChanged");
        }
    }

    // Called when system is running low on memory.
    // Should release unnecessary memory caches.
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (LOCAL_LOG) {
            logger.d(TAG, CLASS_NAME + " onLowMemory");
        }
    }

    /**
     * Setting a SharedPreferences boolean value.
     * 
     * @param key The name of the preference to modify.
     * @param value The new value for the preference.
     */
    public void setBoolean(String key, boolean value) {
        preferencesEditor.putBoolean(key, value);
        preferencesEditor.apply();
    }

    /**
     * Getting a SharedPreferences boolean value.
     * 
     * @param key The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist.
     * @return A boolean value.
     */
    public boolean getBoolean(String key, boolean defValue) {
        boolean value = preferences.getBoolean(key, defValue);
        return value;
    }
}

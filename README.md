# SimpleApp #

This is a an Android application to demonstrate various aspects of best practices in Android code development.


![SimpleApp.PNG](https://bitbucket.org/repo/8Me4xz/images/843743233-SimpleApp.PNG)

### Quick overview of classes ###
(located in /src/ee/motive/simpleapp/)

Base classes:

* /SimpleApp.java – Application class for those who need to maintain global application state.  
* /Config.java – Application configuration class to hold constants.  

Controller classes:

* /controller/MainActivity.java – A simple Activity to list some other activities.
* /controller/MainFragmentActivity.java – A simple FragmentActivity to demonstrate XML view, Java view and a Fragment.
* /controller/SimpleActivity.java – Activity class, to demonstrate various component aspects.
* /controller/SimpleActivityAnnotated.java – Activity class to demonstrate Android Annotations framework capabilities.
* /controller/SimpleFragment.java – A simple Fragment class to use in XML.
* /controller/SimpleService.java – Example Service class.
* /controller/SimpleBroadcastReceiver.java – Example BroadcastReceiver class.

Model classes:

* /model/SimpleData.java – A simple model object.
* /model/SimpleElement.java – An element representing a piece of data.

View classes:

* /view/SimpleTextView.java – Example View class.

Other classes:

* /util/Logger.java – A simple singleton logging class.
* /other/CodeStyleGood.java – A class to demonstrate the coding style guidelines.
* /other/SimpleAsyncTask.java – Example AsyncTask class.
* /other/SimplePreferencesHelper.java – Android Annotations SharedPreferences helper class.